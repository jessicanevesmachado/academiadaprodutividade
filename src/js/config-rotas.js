/* Configurador das rotas */
app.config(function ($stateProvider, $urlRouterProvider,$locationProvider) {
 
    //debugger;
    //$sesso = $cookieStore.get('sessao');
    //$urlRouterProvider.when('/', '/login');    

    $stateProvider
    .state('login', {
      url: '/login',
      templateUrl: 'src/templates/login.html',
      controller: 'LoginController',
      controllerAs: 'LoginCtrl'
    })
    .state('home', {
      url: '/home',
      templateUrl: 'src/templates/home.html',
      controller: 'PrincipalController',
      controllerAs: 'PrincipalCtrl'
    })
    .state('folhaprodutividade', {
      url: '/folhaprodutividade',
      templateUrl: 'src/templates/folhaprodutividade/index.html',
      controller: 'FolhaProdutividadeController',
      cache: false
    });


 
	$urlRouterProvider.otherwise('/login');// quando a url inserida não existe
});