(function () {
    'use strict';
  
    app.factory('folhaProdutividadeService', PrincipalServico);
  
    PrincipalServico.$inject = ['$q', '$http'];
  
    function PrincipalServico($q, $http) {
       var servico = {};
       servico.Salvar = Salvar;
       servico.PorDia = PorDia;
      
  
       return servico;
  
       function Salvar(parametros,callback,erro){
            $http({
                method: 'POST',
                url: urlWebService+'FolhaProdutividade/Salvar',
                data: parametros
            })
            .then(callback)
            .catch(erro);
       }

       function PorDia(parametros,callback,erro){
            $http({
                method: 'POST',
                url: urlWebService+'FolhaProdutividade/PorDia',
                data: parametros
            })
            .then(callback)
            .catch(erro);
        }

       
      
     }
  })();