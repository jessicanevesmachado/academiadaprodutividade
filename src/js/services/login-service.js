(function () {
    'use strict';
  
    app.factory('loginService', loginService);
  
    loginService.$inject = ['$q', '$http'];
  
    function loginService($q, $http) {
       
        var servico = {};
       servico.Autenticar = Autenticar;
       return servico;
  
       function Autenticar(parametros,callback,erro){
            
         $http({
                method: 'POST',
                url: urlWebService+'Login/Autenticar',
                data: parametros
            })
            .then(callback)
            .catch(erro);
       }      
      
     }
  })();