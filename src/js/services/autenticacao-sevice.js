(function () {
    'use strict';
  
    app.factory('autenticacaoService', autenticacaoService);
  
    autenticacaoService.$inject = ['$q', '$http','$cookies','$state'];
  
    function autenticacaoService($q, $http,$cookies,$state)
    {
       
       var servico = {};
       servico.Check = Check;      
       servico.GetSessao = GetSessao;      
      
       return servico;
    
       function GetSessao()
       {

                var usuario = $cookies.get('sessao');

                if(usuario == null || typeof(usuario)== "undefined")
                {
                    $state.go('login');
                    return '';
                }

                return  JSON.parse(usuario);

                 
       }
      
       function Check(){      
       
           var usuarioLogado  = GetSessao();
           if(usuarioLogado == "") return;
           
            $http({
                method: 'POST',
                url: urlWebService+'Login/ValidaToken',
                data: {'token': usuarioLogado.token, 'usuario_id': usuarioLogado.id}
            })
            .then(function(data)
            {
                if(!data.data.sucesso)
                {
                    $cookies.remove('sessao');
                    $state.go('login');
                }

            })
            .catch(function(data){

            });
       } 

       
             
     }
     
  })();