(function () {
    'use strict';
  
    //https://cezarcruz.com.br/como-utilizar-o-http-interceptor-do-angular-js/
    app.controller('FolhaProdutividadeController', FolhaProdutividadeController);
  
    FolhaProdutividadeController.$inject = ['$window','autenticacaoService','$rootScope', '$scope', '$state','folhaProdutividadeService'];
    function FolhaProdutividadeController( $window,autenticacaoService,$rootScope, $scope, $state,folhaProdutividadeService) {
      
      var vm = this; 
      vm.MontarCampos = MontarCampos;
      $scope.Salvar = function()
      {

         var data = $scope.folhaProdutividade.data;
         folhaProdutividadeService.Salvar($scope.folhaProdutividade,function(retorno)
         {
            swal(retorno.data.mensagem);
            $scope.folhaProdutividade = retorno.data.buffer.buffer;
            $scope.folhaProdutividade.data = data;

         },function(erro){
            swal(erro.data.message);
         });
      }

      $scope.PorDia = function(){

        var data = $scope.folhaProdutividade.data;
        if(typeof(data) !== "undefined")
        {
            folhaProdutividadeService.PorDia($scope.folhaProdutividade,function(retorno)
            {
                if(retorno.data.buffer !== null)
                    $scope.folhaProdutividade = retorno.data.buffer;
                else
                {
                    $scope.Limpar(null);
                   // swal("Folha da produtividade do dia "+moment(data).format('DD/MM/YYYY')+" não foi preenchida.");
                }

                $scope.folhaProdutividade.data = data;
                
               

            },function(erro){
                swal(erro.data.message);
            });
        }

      };

      function MontarCampos(quantidade)
      {
         var campos = [quantidade];
      
          for ( var i = 0; i < quantidade; i++) 
          {
              campos[i] =  { prioridade : i, descricao :'',folhaprodutividade_id : 0 }
          }

          return campos;
      }

      $scope.Limpar = function(data)
      {
            var dt = (data==null?moment().format('DD/MM/YYYY'):data);

            $scope.folhaProdutividade = 
            {
              comofoiseudia:0,
              focododia:'',
              oqueaprendihoje:'',
              usuario_id:autenticacaoService.GetSessao().id,
              data: dt,
              gratidoes : vm.MontarCampos(3),
              evitarnodia : vm.MontarCampos(3),
              prioridades : vm.MontarCampos(5)
            };
      } 

      autenticacaoService.Check();
      $scope.Limpar(null);           
      $scope.PorDia();
    }
  })();