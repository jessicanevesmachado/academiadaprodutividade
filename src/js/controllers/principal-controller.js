(function () {
    'use strict';
  
    app.controller('PrincipalController', PrincipalController);
   
    PrincipalController.$inject = ['$cookies' ,'$window','$rootScope', '$scope', '$state','autenticacaoService'];
    function PrincipalController( $cookies, $window,$rootScope, $scope, $state,autenticacaoService)
    {
        
      $scope.Sair = function(){
        $cookies.remove('sessao');
        $state.go('login');
      }

      $scope.IrParaFolhaProdutividade = function(){
          debugger
        $state.go('folhaprodutividade');
      }
      
      autenticacaoService.Check();
    }
  })();