(function () {
    'use strict';
  
    app.controller('LoginController', LoginController);
   
    LoginController.$inject = ['$cookies' ,'$window','$rootScope', '$scope', '$state','loginService'];
    function LoginController( $cookies, $window,$rootScope, $scope, $state,loginService){

      $scope.formulario = 
      {
        email: '',
        senha: ''
      };

      $scope.Autenticar = function (){
      
         loginService.Autenticar($scope.formulario,function(retorno){
           
          if(!retorno.data.sucesso)
            swal(retorno.data.message);
          else
          {
            var sessao = JSON.stringify(retorno.data.buffer);
            $cookies.put('sessao',sessao);  
            $state.go('folhaprodutividade');
          }

         },function(erro){
            swal(erro.data.message);
         });
      }
      
      $scope.EstaLogado = function()
      {
        var usuario = $cookies.get('sessao');

        if(!(usuario == null || typeof(usuario)== "undefined"))
        {
             $state.go('folhaprodutividade');            
        }

      }

      $scope.EstaLogado();
      
      
    }
  })();