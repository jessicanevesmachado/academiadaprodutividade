const _aplicativo = 'Web';
const urlWebService = 'http://192.168.64.2/Projetos/academiadaprodutividade/api/public/';
//const urlWebService = 'http://academiaprodutividade.esy.es/api/public/';
const msgErroRequest = 'Não foi possível concluir a ação,tente novamente.'

var app = angular.module('academiadaprodutividade', ['ngAnimate',
  'ui.bootstrap',
  'ui.router',
  'ngResource',
  'ngCookies',
  'ngSanitize',  
  'dialogs.main',
  'dialogs.default-translations',
  'pascalprecht.translate',  
  'rzModule',
  'ae-datetimepicker', 
  'ui.utils.masks' 
]);

 