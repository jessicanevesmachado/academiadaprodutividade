<?php

use Illuminate\Database\Seeder;

class UsuarioSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('usuario')->insert([
            'nome' => str_random(10),
            'email' => str_random(10).'@gmail.com',
            'ativo' => 1,
            'senha' => bcrypt('1234'),
        ]);
    }
}
