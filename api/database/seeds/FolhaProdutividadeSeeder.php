<?php

use Illuminate\Database\Seeder;
use App\Data\Model\FolhaProdutividade_Gratidao;
use App\Data\Model\FolhaProdutividade_EvitarNoDia;
use App\Data\Model\FolhaProdutividade_Prioridade;
use App\Data\Model\FolhaProdutividade;

class FolhaProdutividadeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    { 
        try{

            $today = date('d/m/Y');
            $today = date('Y-m-d');
            
            DB::beginTransaction();
            $folhaProdutividade = FolhaProdutividade::create([
                'comofoiseudia' => 5,
                'focododia' => 'Saúde',
                'oqueaprendihoje'=> 'Se cuidar',
                'usuario_id' => 1,
                'data' => $today
            ]);
    
            $this->InformaGratidao($folhaProdutividade->id);
            $this->InformaEvitarNoDia($folhaProdutividade->id);
            $this->InformaPrioridades($folhaProdutividade->id);
            DB::commit();
        }
        catch(Exception $e)
        {
            DB::rollBack();
        }
       
    }

    public function InformaGratidao($idFolhaProdutividade){

         $gratidao1 = FolhaProdutividade_Gratidao::create([
            'prioridade'=>'1',
            'descricao'=>'Familia',
            'folhaprodutividade_id'=>$idFolhaProdutividade
        ]);
        
         
        $gratidao2 = FolhaProdutividade_Gratidao::create([
            'prioridade'=>'2',
            'descricao'=>'Amigos',
            'folhaprodutividade_id'=>$idFolhaProdutividade
        ]);

        $gratidao3 = FolhaProdutividade_Gratidao::create([
            'prioridade'=>'3',
            'descricao'=>'Trabalho',
            'folhaprodutividade_id'=>$idFolhaProdutividade
        ]);
    }

    public function InformaEvitarNoDia($idFolhaProdutividade){
       
        $EvitarNoDia1 = FolhaProdutividade_EvitarNoDia::create([
           'prioridade'=>'1',
           'descricao'=>'Raiva',
           'folhaprodutividade_id'=>$idFolhaProdutividade
       ]);
       
        
       $EvitarNoDia2 = FolhaProdutividade_EvitarNoDia::create([
           'prioridade'=>'2',
           'descricao'=>'Medo',
           'folhaprodutividade_id'=>$idFolhaProdutividade
       ]);

       $EvitarNoDia3 = FolhaProdutividade_EvitarNoDia::create([
           'prioridade'=>'3',
           'descricao'=>'Não Deixar os outros te fazer de idiota',
           'folhaprodutividade_id'=>$idFolhaProdutividade
       ]);
   }

   public function InformaPrioridades($idFolhaProdutividade)
   {
           
            $prioridade1 = FolhaProdutividade_Prioridade::create([
            'prioridade'=>'1',
            'descricao'=>'Organização',
            'folhaprodutividade_id'=>$idFolhaProdutividade
        ]);
        
            
        $prioridade2 = FolhaProdutividade_Prioridade::create([
            'prioridade'=>'2',
            'descricao'=>'Folha da produtividade',
            'folhaprodutividade_id'=>$idFolhaProdutividade
        ]);

        $prioridade3 = FolhaProdutividade_Prioridade::create([
            'prioridade'=>'3',
            'descricao'=>'Redes sociais',
            'folhaprodutividade_id'=>$idFolhaProdutividade
        ]);
        
        $prioridade4 = FolhaProdutividade_Prioridade::create([
            'prioridade'=>'4',
            'descricao'=>'Redes sociais',
            'folhaprodutividade_id'=>$idFolhaProdutividade
        ]);

        $prioridade5 = FolhaProdutividade_Prioridade::create([
            'prioridade'=>'5',
            'descricao'=>'Aborrecimentos',
            'folhaprodutividade_id'=>$idFolhaProdutividade
        ]);
    }
}
