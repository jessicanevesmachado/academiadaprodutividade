<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFolhaProdutividadeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('folhaprodutividade', function (Blueprint $table) {
           
            $table->increments('id');

            $table->integer('usuario_id')->unsigned();
            $table->foreign('usuario_id')->references('id')->on('usuario'); 
            $table->string('focododia',150)->nullable();
            $table->integer('comofoiseudia')->nullable();
            $table->string('oqueaprendihoje',300)->nullable();
            $table->date('data');
            $table->unique(['data','usuario_id']);
            $table->timestamps();
            $table->softDeletes();
        });
    }
  
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('folhaprodutividade');
    }
}
