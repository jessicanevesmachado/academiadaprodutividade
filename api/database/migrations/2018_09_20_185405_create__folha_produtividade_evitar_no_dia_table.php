<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFolhaProdutividadeEvitarNoDiaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('folhaprodutividadeevitarnodia', function (Blueprint $table) {
            $table->increments('id');
            $table->string('descricao',150);
            $table->integer('prioridade'); 

            $table->integer('folhaprodutividade_id')->unsigned();
            $table->foreign('folhaprodutividade_id')->references('id')->on('folhaprodutividade');


            
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('folhaprodutividadeevitarnodia');
    }
}
