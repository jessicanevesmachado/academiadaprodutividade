<?php

namespace App\Service;
use App\Data\Repository;
use App\Service\Contracts\ILoginService;
use DB;

class LoginService implements ILoginService
{ 
 

    function Autenticar($parametros)
    {
        try
        {             
            return app('App\Data\Repository\LoginRepository')->Autenticar($parametros);
        }
        catch(Exception $e)
        {            
             return array('sucesso'=>true, 'mensagem'=> $e->getMessage());
        } 
      
    }
    
    function ValidaToken($parametros)
    {
        try
        {             
            return app('App\Data\Repository\LoginRepository')->ValidaToken($parametros);
        }
        catch(Exception $e)
        {            
             return array('sucesso'=>true, 'mensagem'=> $e->getMessage());
        } 
      
    }
}