<?php

namespace App\Service;
use App\Data\Repository;
use App\Service\Contracts\IFolhaProdutividadeService;
use DB;

class FolhaProdutividadeService implements IFolhaProdutividadeService
{ 

    function Salvar($formulario)
    {
        try
        {             
            return app('App\Data\Repository\FolhaProdutividadeRepository')->Salvar($formulario);
        }
        catch(Exception $e)
        {            
             return array('sucesso'=>true, 'mensagem'=> $e->getMessage());
        } 
       
    }

    function BuscaPorData($parametros)
    {
        try
        {             
            return app('App\Data\Repository\FolhaProdutividadeRepository')->BuscaPorData($parametros);
        }
        catch(Exception $e)
        {            
             return array('sucesso'=>true, 'mensagem'=> $e->getMessage());
        } 
      
    }
}