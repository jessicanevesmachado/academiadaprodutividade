<?php

namespace App\Service\Contracts;

interface IFolhaProdutividadeService
{
    public function Salvar($formulario);
    public function BuscaPorData($parametros);
 
}

 