<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use app\Service;

class LoginController  extends Controller
{
     
    public function Autenticar(Request $request)
    { 
         
        $response =  app('App\Service\LoginService')->Autenticar($request->json()->all());
        return response()->json( $response);  
         
    }

    function ValidaToken(Request $request)
    {
         
         $response =  app('App\Service\LoginService')->ValidaToken($request->json()->all());
         return response()->json( $response);           
    }

     
}
