<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Data\Model\FolhaProdutividade;

class FolhaProdutividadeController extends Controller
{
    public function Listar()
    {
        $folhaProdutividade = FolhaProdutividade::all();
        return response()->json($folhaProdutividade); 
    }

    public function PorDia(Request $request)
    {        
        $response =  app('App\Service\FolhaProdutividadeService')->BuscaPorData($request->json()->all());
        return response()->json( $response); 
    }

    
    public function Salvar(Request $request)
    { 
        $response =  app('App\Service\FolhaProdutividadeService')->Salvar($request->json()->all());
        return response()->json( $response); 
    }
}
