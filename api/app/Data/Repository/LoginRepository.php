<?php

namespace App\Data\Repository;
use App\Data\Model\Usuario;
use App\Data\Model\Token;
use App\Data\Repository\Contracts\ILoginRepository;
use DB;

class LoginRepository implements ILoginRepository
{
    function Autenticar($parametros)
    {
        try
        {             
           $usuario = Usuario::where('email', $parametros['email'])->where('senha', $parametros['senha']);
           
           if($usuario->count() > 0)
           {                 
                $usuario = $usuario->first();             
                
                $token   = Token::Gerar($usuario->id);
                $usuario = array('token'=>$token->token,'nome'=>$usuario->nome,'id'=>$usuario->id);
                return array('sucesso'=>true, 'message'=> 'autenticado.', 'buffer' => $usuario);
           }
            
            return array('sucesso'=>false, 'message'=> 'Usuário ou senha incorreto.');
        }
        catch(Exception $e)
        {            
             return array('sucesso'=>false, 'mensagem'=> $e->getMessage());
        } 
      
    }

    function ValidaToken($parametros)
    {
           $date = DB::select('SELECT   NOW() Data;')[0]->Data;   

           $token = Token::where('usuario_id', $parametros['usuario_id'])->where('token', $parametros['token'])->where('data_expiracao', '>',  $date);
           
           if($token->count() > 0)
           {                 
             return array('sucesso'=>true);
           }

           return array('sucesso'=>false);
    }
}