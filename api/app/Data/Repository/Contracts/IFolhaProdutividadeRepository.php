<?php

namespace App\Data\Repository\Contracts;

interface IFolhaProdutividadeRepository
{
    public function Salvar($formulario);
    public function BuscaPorData($data);
 
}

 