<?php

namespace App\Data\Repository;
use App\Data\Model\FolhaProdutividade;
use App\Data\Repository\Contracts\IFolhaProdutividadeRepository;
use DB;

class FolhaProdutividadeRepository implements IFolhaProdutividadeRepository
{

    function Salvar($formulario)
    {
        try
        {
             DB::beginTransaction();    
             $folhaProdutividade =  app('App\Data\Model\FolhaProdutividade')->CriarFolhaProdutividade($formulario); 
             app('App\Data\Model\FolhaProdutividade_Gratidao')->InformaGratidao($formulario['gratidoes'], $folhaProdutividade->id);
             app('App\Data\Model\FolhaProdutividade_EvitarNoDia')->InformaEvitarNoDia($formulario['evitarnodia'],$folhaProdutividade->id);
             app('App\Data\Model\FolhaProdutividade_Prioridade')->InformaPrioridade($formulario['prioridades'],$folhaProdutividade->id);            
             DB::commit();
             return array('sucesso'=>true, 'mensagem'=> "Operação realizada com sucesso.", 'buffer'=> $this->BuscaPorData($formulario));
        }
        catch (\Illuminate\Database\QueryException $e)
        {
             DB::rollBack();
             return array('sucesso'=>true, 'mensagem'=> $e->getMessage());
        } 
       
    }

    function BuscaPorData($parametros)
    {
        try
        {
            $data = date('Y-m-d', strtotime(str_replace('/', '-', $parametros['data']))); 

            $folhaProdutividade = 
                                FolhaProdutividade::where('data', $data)->where('usuario_id',$parametros['usuario_id'])
                                ->with('gratidoes')
                                ->with('evitarnodia')
                                ->with('prioridades')
                                ->first();
          
            /* foreach ($folhaProdutividade as $f) {
                $teste =  $f->Gratidao;
            }*/

            return array('sucesso'=>true, 'mensagem'=> 'Operação realizada com sucesso', 'buffer'=>$folhaProdutividade);
            
        }
        catch (\Illuminate\Database\QueryException $e)
        {
            DB::rollBack();
            return array('sucesso'=>true, 'mensagem'=> $e->getMessage());
        } 
    }
}