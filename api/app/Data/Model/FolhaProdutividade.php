<?php

namespace App\Data\Model;

use Illuminate\Database\Eloquent\Model;
use App\Data\Model\FolhaProdutividade_Gratidao;
use App\Data\Model\FolhaProdutividade_EvitarNoDia;
use App\Data\Model\FolhaProdutividade_Prioridade;
use DB;

class FolhaProdutividade extends Model
{ 
    
    protected $table = 'folhaprodutividade';
    protected $fillable = array('comofoiseudia','focododia','oqueaprendihoje','usuario_id','data');
    protected $guarded = ['id', 'created_at', 'update_at'];
     

    function CriarFolhaProdutividade($formulario)
    {
         $atualizar = isset($formulario['id']);
         
        if ($atualizar)
                $folhaProdutividade = $this->Atualizar($formulario);
        else
                $folhaProdutividade = $this->Inserir($formulario);

        
        return $folhaProdutividade;
    }

    function Inserir($formulario)
    {
         
        $var     =  $formulario['data'];
        $date    = str_replace('/', '-', $var);
        $newDate = date('Y-m-d', strtotime($date));
         

        $folhaProdutividade = FolhaProdutividade::create([
            'comofoiseudia'   => $formulario['comofoiseudia'],
            'focododia'       => $formulario['focododia'],
            'oqueaprendihoje' => $formulario['oqueaprendihoje'],
            'usuario_id'      => $formulario['usuario_id'],
            'data'            => $newDate
        ]); 
        
        return $folhaProdutividade;
    }

    function Atualizar($formulario)
    {
        $folhaProdutividade = FolhaProdutividade::find($formulario['id']);
        $folhaProdutividade->comofoiseudia   =   $formulario['comofoiseudia'];
        $folhaProdutividade->focododia       =  $formulario['focododia'];
        $folhaProdutividade->oqueaprendihoje =  $formulario['oqueaprendihoje'];        
        $folhaProdutividade->save();

        return $folhaProdutividade;
    }

    public function gratidoes()
    {
        return $this->hasMany('App\Data\Model\FolhaProdutividade_Gratidao', 'folhaprodutividade_id');
       
    }

    public function evitarnodia()
    {
        return $this->hasMany('App\Data\Model\FolhaProdutividade_EvitarNoDia', 'folhaprodutividade_id');
       
    }

    public function prioridades()
    {
        return $this->hasMany('App\Data\Model\FolhaProdutividade_Prioridade', 'folhaprodutividade_id');
       
    }
}


