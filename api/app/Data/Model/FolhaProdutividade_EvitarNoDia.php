<?php

namespace App\Data\Model;

use Illuminate\Database\Eloquent\Model;

class FolhaProdutividade_EvitarNoDia extends Model
{
    protected $table = 'folhaprodutividadeevitarnodia';
    protected $fillable = array('descricao','prioridade','folhaprodutividade_id');
    protected $guarded = ['id'];
    public $timestamps = false; // para desativar colunas de data obrigatoria pelo eloquent
    
    function InformaEvitarNoDia($evitarnodia,$folhaprodutividade_id)
    {
        foreach ($evitarnodia as $evitar)
        {           
            $dbEvitar = FolhaProdutividade_EvitarNoDia::where('folhaprodutividade_id', $folhaprodutividade_id)->where('prioridade', $evitar['prioridade'])->count(); 
            
            if( $dbEvitar == 0)
                    $this->Inserir($evitar,$folhaprodutividade_id);
            else
                    $this->Atualizar($evitar, $folhaprodutividade_id);
        }
    }

    function Inserir($evitar,$folhaprodutividade_id)
    {

        $descricao = ($evitar['descricao'] == null ? " " : $evitar['descricao']);

        FolhaProdutividade_EvitarNoDia::create([
            'prioridade'=>$evitar['prioridade'],
            'descricao'=> $descricao ,
            'folhaprodutividade_id'=>$folhaprodutividade_id
        ]);
    }

    function Atualizar($evitar,$folhaprodutividade_id)
    { 
        if(!isset($evitar['descricao']))
          $evitar['descricao'] = "";

            $descricao = ($evitar['descricao'] == null ? " " : $evitar['descricao']);
            FolhaProdutividade_EvitarNoDia::where('folhaprodutividade_id', $folhaprodutividade_id)
          ->where('prioridade', $evitar['prioridade'])
          ->update(['descricao' =>  $descricao ]); 
        
    }


    
}
