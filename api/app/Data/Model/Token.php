<?php

namespace App\Data\Model;
use DB;
use Illuminate\Database\Eloquent\Model;

class Token extends Model
{
    protected $table = 'token'; 
    protected $fillable = array('token','usuario_id','data_Expiracao');

    public static function Gerar($usuario_id)
    {
       
       Token::ExcluirTokenDoUsuario($usuario_id);
       $date = DB::select('SELECT   NOW() + INTERVAL 30 MINUTE Data;')[0]->Data;
       
       
       $token = Token::create([
            'token'          => base64_encode(random_bytes(64)),
            'usuario_id'     => $usuario_id,
            'data_Expiracao' => $date            
       ]);
       
       

        return $token;
    }

    public static function ExcluirTokenDoUsuario($usuario_id)
    {
        Token::where('usuario_id',$usuario_id)->delete();
    }
}
