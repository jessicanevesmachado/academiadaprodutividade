<?php

namespace App\Data\Model;

use Illuminate\Database\Eloquent\Model;

class FolhaProdutividade_Prioridade extends Model
{
    protected $table = 'folhaprodutividadeprioridade';
    protected $fillable = array('descricao','prioridade','folhaprodutividade_id');
    protected $guarded = ['id'];
    public $timestamps = false; // para desativar colunas de data obrigatoria pelo eloquent
    
    function InformaPrioridade($prioridades, $folhaprodutividade_id)
    {
        foreach ($prioridades as $prioridade)
        {           
            $dbPrioridade = FolhaProdutividade_Prioridade::where('folhaprodutividade_id', $folhaprodutividade_id)->where('prioridade', $prioridade['prioridade'])->count(); 
            if($dbPrioridade == 0)
                    $this->Inserir($prioridade,$folhaprodutividade_id);
            else
                    $this->Atualizar($prioridade, $folhaprodutividade_id);
        }
    }

    function Inserir($prioridade,$folhaprodutividade_id)
    {
        $descricao = ($prioridade['descricao'] == null ? " " : $prioridade['descricao']);
        FolhaProdutividade_Prioridade::create([
            'prioridade'=>$prioridade['prioridade'],
            'descricao'=>$descricao ,
            'folhaprodutividade_id'=>$folhaprodutividade_id
        ]);
    }

    function Atualizar($prioridade,$folhaprodutividade_id)
    {
       
        if(!isset($prioridade['descricao']))
          $prioridade['descricao'] = "";

        $descricao = ($prioridade['descricao'] == null ? " " : $prioridade['descricao']);
        FolhaProdutividade_Prioridade::where('folhaprodutividade_id', $folhaprodutividade_id)
         ->where('prioridade', $prioridade['prioridade'])
         ->update(['descricao' =>$descricao]); 
    }
}
