<?php

namespace App\Data\Model;

use Illuminate\Database\Eloquent\Model;
use App\FolhaProdutividade;

class FolhaProdutividade_Gratidao extends Model
{
    protected $table = 'folhaprodutividadegratidao';
    protected $fillable = array('descricao','prioridade','folhaprodutividade_id');
    protected $guarded = ['id'];
    public $timestamps = false; // para desativar colunas de data obrigatoria pelo eloquent
    
  
    function InformaGratidao($gratidoes,$folhaprodutividade_id)
    {

        foreach ($gratidoes as $gratidao)
        {       
            $dbGratidao = FolhaProdutividade_Gratidao::where('folhaprodutividade_id', $folhaprodutividade_id)->where('prioridade', $gratidao['prioridade'])->count();
            if( $dbGratidao == 0)
                    $this->Inserir($gratidao,$folhaprodutividade_id);
            else
                    $this->Atualizar($gratidao, $folhaprodutividade_id);
           
        }
       
    }

    function Inserir($gratidao,$folhaprodutividade_id)
    {
           $descricao = ($gratidao['descricao'] == null ? " " : $gratidao['descricao']);
            FolhaProdutividade_Gratidao::create([
                'prioridade'=>$gratidao['prioridade'],
                'descricao'=>  $descricao ,
                'folhaprodutividade_id'=>$folhaprodutividade_id
            ]);
        
    }

    function Atualizar($gratidao,$folhaprodutividade_id)
    {
            if(!isset($gratidao['descricao']))
                 $gratidao['descricao'] = "";
   
            $descricao = ($gratidao['descricao'] == null ? " " : $gratidao['descricao']);
            FolhaProdutividade_Gratidao::where('folhaprodutividade_id', $folhaprodutividade_id)
            ->where('prioridade', $gratidao['prioridade'])
            ->update(['descricao' =>   $descricao]); 
        
    }

    public function FolhaProdutividade()
    {
        //return $this->belongsTo('App\FolhaProdutividade','folhaprodutividade_id');
        return $this->hasMany(FolhaProdutividade::class);
	}
}
