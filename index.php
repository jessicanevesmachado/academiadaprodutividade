<!doctype html>
<html lang="en"  ng-app="academiadaprodutividade">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
 
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="src/css/style.css">

    <link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
    <link rel="stylesheet" href="src/css/login.css">

    <title>Academia da Produtividade</title>
  </head>
  <body>    

    <div if-loading class='backModalLoading'>
      <div class='loading'></div>
    </div>  

    <main role="main" class="container container-first">
      <div class="starter-template ">
          
        <ui-view></ui-view>   
          
      </div>
    </main>

    <?php 
          $random = '?id='.rand(5, 15);
    ?>
   
 
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="bower_components/jquery/dist/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script src="bower_components/bootstrap/dist/js/bootstrap.min.js" ></script>


      <!-- angular -->
    <script src="bower_components/angular/angular.js"></script>    
    <!-- ui-router -->
    <script src="bower_components/angular-ui-router/release/angular-ui-router.js"></script>
    <!-- angular-resouce -->
    <script src="bower_components/angular-resource/angular-resource.js"></script>
    <!-- angular-cookies -->
    <script src="bower_components/angular-cookies/angular-cookies.js"></script>
    <!-- angular-animate -->
    <script src="bower_components/angular-animate/angular-animate.js"></script>
    <!-- angular-sanitize -->
    <script src="bower_components/angular-sanitize/angular-sanitize.js"></script>
    <!-- angular-bootstrap -->
    <script src="bower_components/angular-bootstrap/ui-bootstrap-tpls.js"></script>
    <!-- angular-translate -->
    <script src="bower_components/angular-translate/angular-translate.js"></script>
    <!-- angular-i18n -->
    <script src="bower_components/angular-i18n/angular-locale_pt-br.js"></script>
    <!-- angular-dialog-service -->
    <script src="bower_components/angular-dialog-service/dist/dialogs.js"></script>
    <script src="bower_components/angular-dialog-service/dist/dialogs-default-translations.js"></script>
  
 

    <!-- angular-slider -->
    <script src="bower_components/angularjs-slider/dist/rzslider.js"></script>
    <!-- moment -->
    <script src="bower_components/moment/min/moment-with-locales.js"></script>
    <script src="bower_components/moment/locale/pt-br.js"></script>
    <!-- eonasdan-bootstrap-datetimepicker -->
    <script src="bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
    <script src="bower_components/angular-eonasdan-datetimepicker/dist/angular-eonasdan-datetimepicker.js"></script>
  
    <!--Input Mask-->
    <script src="bower_components/angular-input-masks/angular-input-masks-standalone.min.js"></script>
 
    <script src="src/js/index.js<?=$random?>"></script>
    <script src="src/js/config-rotas.js<?=$random?>"></script>
 


    <!--servico-->
    <script src="src/js/services/folhaprodutividade-service.js<?=$random?>"></script>
    <script src="src/js/services/login-service.js<?=$random?>"></script>
    <script src="src/js/services/autenticacao-sevice.js<?=$random?>"></script>



    <!--controler-->
    <script src="src/js/controllers/folhaprodutividade-controller.js<?=$random?>"></script>
    <script src="src/js/controllers/login-controller.js<?=$random?>"></script>
    <script src="src/js/controllers/principal-controller.js<?=$random?>"></script>



    <!--diretivas-->
    <script src="src/diretivas/plugins.js<?=$random?>"></script>

    

    <script src="https://cdn.jsdelivr.net/npm/gijgo@1.9.10/js/gijgo.min.js" type="text/javascript"></script>
    <link href="https://cdn.jsdelivr.net/npm/gijgo@1.9.10/css/gijgo.min.css" rel="stylesheet" type="text/css" />
    <script src="src/js/plugins/datetimepicker/bootstrap-datepicker.pt-BR.js.js" charset="UTF-8"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
  </body>
</html>

